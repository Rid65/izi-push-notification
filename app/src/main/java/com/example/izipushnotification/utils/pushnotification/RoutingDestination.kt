package com.example.izipushnotification.utils.pushnotification

import android.os.Parcelable

interface RoutingDestination : Parcelable {
    var popUpDestinationId: Int?
    var popUpInclusive: Boolean?
}