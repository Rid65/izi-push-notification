package com.example.izipushnotification.utils.pushnotification

import androidx.navigation.NavController

/** Цепочка обработчиков пуш-уведомления.
 * Каждый обработчик последовательно пытается обработать запрос и
 * в случае неудачи передает запрос следующему обработчику.
 * Последним обработчиком всегда должен быть LastNotificationHandler,
 * чтобы открыть главный экран приложения, если ни один другой обработчик не смог обработать запрос */
class PushNotificationHandlerChain : PushNotificationHandler {
    val chain: MutableList<BasePushNotificationHandler> by lazy {
        val list = mutableListOf(
            DashboardNotificationHandler(),
            InternalBankCardNotificationHandler(),
            LeClickOrderNotificationHandler(),
            MoviesOrderNotificationHandler(),
            KupiKuponOrderNotificationHandler(),
            YClientOrderNotificationHandler(),
            DigitalBazarOrderNotificationHandler(),
            TanukiOrderNotificationHandler(),
            LastNotificationHandler()
        )
        list.forEachIndexed { key, value ->
            if (key + 1 < list.size) value.next = list[key + 1]
        }
        list
    }

    override fun handle(args: PushNotificationHelperFragmentArgs, navController: NavController) {
        chain.first().handle(args, navController)
    }
}
