package com.example.izipushnotification.utils.pushnotification

import androidx.navigation.NavController

interface PushNotificationHandler {
    /** Метод, который выполняет обработку пуш-уведомления */
    fun handle(args: PushNotificationHelperFragmentArgs, navController: NavController)
}

/** Базовый обработчик пуш-уведомления.*/
abstract class BasePushNotificationHandler(var next: PushNotificationHandler? = null) : PushNotificationHandler {
    /** Метод определяет сможет ли текущий обработчик обработать уведомление */
    protected abstract fun canHandle(args: PushNotificationHelperFragmentArgs): Boolean
    /** Метод предоставляет пункт назначения для навигации */
    protected abstract fun getDestination(args: PushNotificationHelperFragmentArgs): RoutingDestination?
    override fun handle(args: PushNotificationHelperFragmentArgs, navController: NavController) {
        if (!canHandle(args)) {
            next?.handle(args, navController)
            return
        }
        navController.navigate(
            PushNotificationHelperFragmentDirections.actionPushNotificationHelperFragmentToDashboardFragment(
                getDestination(args)
            )
        )
    }
}

/** Специальный обработчик, который всегда открывает главный экран.
 * Должен использоваться в качестве последнего обработчика на случай,
 * если ни один другой не смог обработать запрос */
class LastNotificationHandler(next: PushNotificationHandler? = null) :
    BasePushNotificationHandler(next) {
    override fun canHandle(args: PushNotificationHelperFragmentArgs) = true
    override fun getDestination(args: PushNotificationHelperFragmentArgs): RoutingDestination? = null
}

class DashboardNotificationHandler(next: PushNotificationHandler? = null) :
    BasePushNotificationHandler(next) {
    override fun canHandle(args: PushNotificationHelperFragmentArgs): Boolean {
        return args.type == NotificationType.INFO
    }
    override fun getDestination(args: PushNotificationHelperFragmentArgs): RoutingDestination? = null
}

class InternalBankCardNotificationHandler(next: PushNotificationHandler? = null) :
    BasePushNotificationHandler(next) {
    override fun canHandle(args: PushNotificationHelperFragmentArgs): Boolean {
        return args.type == NotificationType.BANKING || args.type == NotificationType.TRANSACTION
    }
    override fun getDestination(args: PushNotificationHelperFragmentArgs): RoutingDestination? {
        return InternalCardDestination()
    }
}

class LeClickOrderNotificationHandler(next: PushNotificationHandler? = null) :
    BasePushNotificationHandler(next) {
    override fun canHandle(args: PushNotificationHelperFragmentArgs): Boolean {
        return args.type == NotificationType.ORDER &&
                args.miniAppId == MiniAppId.LECLICK &&
                !args.id.isNullOrEmpty()
    }
    override fun getDestination(args: PushNotificationHelperFragmentArgs): RoutingDestination? {
        return RestaurantOrderDestination(args.id.orEmpty()).apply {
            popUpInclusive = false
        }
    }
}

class MoviesOrderNotificationHandler(next: PushNotificationHandler? = null) :
    BasePushNotificationHandler(next) {
    override fun canHandle(args: PushNotificationHelperFragmentArgs): Boolean {
        return args.type == NotificationType.ORDER &&
                args.miniAppId == MiniAppId.KINOHOD &&
                !args.id.isNullOrEmpty()
    }
    override fun getDestination(args: PushNotificationHelperFragmentArgs): RoutingDestination? {
        return MovieOrderSuccessDestination(args.id.orEmpty()).apply {
            popUpInclusive = false
        }
    }
}

class KupiKuponOrderNotificationHandler(next: PushNotificationHandler? = null) :
    BasePushNotificationHandler(next) {
    override fun canHandle(args: PushNotificationHelperFragmentArgs): Boolean {
        return args.type == NotificationType.ORDER &&
                args.miniAppId == MiniAppId.KUPIKUPON &&
                !args.id.isNullOrEmpty()
    }
    override fun getDestination(args: PushNotificationHelperFragmentArgs): RoutingDestination? {
        return KupiKuponOrderDestination(args.id.orEmpty()).apply {
            popUpInclusive = false
        }
    }
}

class YClientOrderNotificationHandler(next: PushNotificationHandler? = null) :
    BasePushNotificationHandler(next) {
    override fun canHandle(args: PushNotificationHelperFragmentArgs): Boolean {
        return args.type == NotificationType.ORDER &&
                args.miniAppId == MiniAppId.YCLIENTS &&
                !args.id.isNullOrEmpty()
    }
    override fun getDestination(args: PushNotificationHelperFragmentArgs): RoutingDestination? {
        return YClientsOrderDestination(args.id.orEmpty()).apply {
            popUpInclusive = false
        }
    }
}

class DigitalBazarOrderNotificationHandler(next: PushNotificationHandler? = null) :
    BasePushNotificationHandler(next) {
    override fun canHandle(args: PushNotificationHelperFragmentArgs): Boolean {
        return args.type == NotificationType.ORDER &&
                args.miniAppId == MiniAppId.DIGITAL_BAZAR &&
                !args.id.isNullOrEmpty()
    }
    override fun getDestination(args: PushNotificationHelperFragmentArgs): RoutingDestination? {
        return DigitalBazarOrderDestination(args.id.orEmpty()).apply {
            popUpInclusive = false
        }
    }
}

class TanukiOrderNotificationHandler(next: PushNotificationHandler? = null) :
    BasePushNotificationHandler(next) {
    override fun canHandle(args: PushNotificationHelperFragmentArgs): Boolean {
        return args.type == NotificationType.ORDER &&
                args.miniAppId == MiniAppId.TANUKI &&
                !args.id.isNullOrEmpty()
    }
    override fun getDestination(args: PushNotificationHelperFragmentArgs): RoutingDestination? {
        return TanukiOrderDestination(args.id.orEmpty()).apply {
            popUpInclusive = false
        }
    }
}
