package com.example.izipushnotification.utils.pushnotification

import android.app.Activity
import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

/**
 * Фрагмент отвечает за обработку push-уведомлений
 * */
class PushNotificationHelperFragment : Fragment() {

    @Inject lateinit var analyticHelper: AnalyticHelper
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: CheckAuthViewModel by viewModels { viewModelFactory }
    private val args: PushNotificationHelperFragmentArgs by navArgs()

    @Inject
    lateinit var handlerChain: PushNotificationHandlerChain

    @SuppressWarnings("deprecation")
    override fun onAttach(activity: Activity) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            AndroidSupportInjection.inject(this)
        }
        super.onAttach(activity)
    }

    override fun onAttach(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            AndroidSupportInjection.inject(this)
        }
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.sessionWasUpdatedLiveData.observe(this, Observer {
            handleNotification()
        })
    }

    private fun handleNotification() {
        analyticHelper.logEvent(AnalyticEventNames.EVENT_PUSH_CLICK)
        handlerChain.handle(args, findNavController())
    }
}
