package com.example.izipushnotification.utils.pushnotification

enum class NotificationType(val code: String) {
    INFO("info"),
    TRANSACTION("transaction"),
    CODE_CONFIRMATION("code_confirmation"),
    ORDER("order"),
    BANKING("banking");

    companion object {
        fun valueByCode(code: String): NotificationType? {
            return values().firstOrNull { it.code == code }
        }
    }
}
