package com.example.izipushnotification.utils.pushnotification

import androidx.lifecycle.LiveData
import javax.inject.Inject

class CheckAuthViewModel @Inject constructor(
    private val accessTokenStorage: AccessTokenStorage,
    private val eventBus: EventBus
) : BaseViewModel() {

    private val _sessionWasUpdatedLiveEvent = SingleLiveEvent<Empty>()
    val sessionWasUpdatedLiveData: LiveData<Empty> = _sessionWasUpdatedLiveEvent

    init {
        if (accessTokenStorage.isTokenAlive()) {
            _sessionWasUpdatedLiveEvent.postValue(Empty())
        } else {
            eventBus.post(SessionExpiredEvent(SessionInterception.CHECK_PIN))
        }

        eventBus.observe(SessionUpdatedEvent::class).execute { result ->
            result.doOnSuccess {
                _sessionWasUpdatedLiveEvent.postValue(Empty())
            }
        }
    }
}
